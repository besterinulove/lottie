package com.besterinulove.kotlin.lottietest

import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEachIndexed
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.airbnb.lottie.LottieCompositionFactory
import com.airbnb.lottie.LottieDrawable
import com.google.android.material.bottomnavigation.BottomNavigationItemView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        val homeDrawable = getDrawable("home.json")
        val dashboardDrawable = getDrawable("sportGame.json")
        val notificationsDrawable = getDrawable("user.json")
        val animationDrawables = listOf(homeDrawable, dashboardDrawable, notificationsDrawable)

        navView.menu.forEachIndexed { index, item ->
            item.icon = animationDrawables[index]
        }

        navView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> {
                    homeDrawable.playAnimation()
                    true
                }
                R.id.navigation_dashboard->{
                    dashboardDrawable.playAnimation()
                    true
                }
                R.id.navigation_notifications->{
                    notificationsDrawable.playAnimation()
                    true
                }
                else -> false
            }
        }
    }

    fun getDrawable(fileName: String): LottieDrawable {
        val lottieDrawable = LottieDrawable()
        LottieCompositionFactory.fromAsset(this, fileName).addListener {
            lottieDrawable.composition = it
        }
        return lottieDrawable
    }
}
